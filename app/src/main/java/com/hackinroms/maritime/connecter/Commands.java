package com.hackinroms.maritime.connecter;


public class Commands {

//    public static final String HELM="HELM = ";
//    public static final String LEFT_TELEGRAPH="LEFT_TELEGRAPH = ";
//    public static final String RIGHT_TELEGRAPH="RIGHT_TELEGRAPH = ";
//
//    // Thruster Panel Commands
//    public static final String T_BOW_RED_FULL="BowRedFull";
//    public static final String T_BOW_RED_HALF="BowRedHALF";
//    public static final String T_BOW_STOP="BowStop";
//    public static final String T_BOW_GREEN_HALF="BowGreenHalf";
//    public static final String T_BOW_GREEN_FULL="BowGreenFull";
//    public static final String T_MOTOR_STOP="MotorStop";
//    public static final String T_MOTOR_START="MotorStart";
//    public static final String T_STERN_RED_FULL="SternRedFull";
//    public static final String T_STERN_RED_HALF="SternRedHALF";
//    public static final String T_STERN_STOP="SternStop";
//    public static final String T_STERN_GREEN_HALF="SternGreenHalf";
//    public static final String T_STERN_GREEN_FULL="SternGreenFull";
//
//    // Winch Panel Commands
//    public static final String W_SPEED_1="Speed 1";
//    public static final String W_SPEED_2="Speed 2";
//    public static final String W_SPEED_3="Speed 3";
//    public static final String W_SPEED_4="Speed 4";
//    public static final String W_EMERGE_RELEASE="EMERGE_RELEASE";
//    public static final String W_BREAK="BREAK";
//    public static final String W_AUTO="AUTO";
//    public static final String W_MAN="MAN";
//    public static final String W_CONSAT_RPM="CONSAT_RPM";
//    public static final String W_COMB="COMB";
//    public static final String W_STERV_VIEW="STERV_VIEW";
//    public static final String W_WINCH_LEVEL="WINCH_LEVEL = ";
//
//    // Camera Panel Commands
//    public static final String C_LEFT_UP="LEFT_UP";
//    public static final String C_LEFT="LEFT";
//    public static final String C_LEFT_DOWN="LEFT_DOWN";
//    public static final String C_UP="UP";
//    public static final String C_DOWN="DOWN";
//    public static final String C_RIGHT_UP="RIGHT_UP";
//    public static final String C_RIGHT="RIGHT";
//    public static final String C_RIGHT_DOWN="RIGHT_DOWN";
//    public static final String C_PORT_WING="PORT_WING";
//    public static final String C_STBD_WING="STBD_WING";
//    public static final String C_BRDG="BRDG";
//    public static final String C_PITCH="PITCH";
//    public static final String C_RESET="RESET";



    ////////////////////////////////

    public static final int HELM=1;
    public static final int LEFT_TELEGRAPH=2;
    public static final int RIGHT_TELEGRAPH=3;

    // Thruster Panel Commands
    public static final int T_BOW_RED_FULL=4;
    public static final int T_BOW_RED_HALF=5;
    public static final int T_BOW_STOP=6;
    public static final int T_BOW_GREEN_HALF=7;
    public static final int T_BOW_GREEN_FULL=8;
    public static final int T_MOTOR_STOP=9;
    public static final int T_MOTOR_START=10;
    public static final int T_STERN_RED_FULL=11;
    public static final int T_STERN_RED_HALF=12;
    public static final int T_STERN_STOP=13;
    public static final int T_STERN_GREEN_HALF=14;
    public static final int T_STERN_GREEN_FULL=15;

    // Winch Panel Commands
    public static final int W_SPEED_1=16;
    public static final int W_SPEED_2=17;
    public static final int W_SPEED_3=18;
    public static final int W_SPEED_4=19;
    public static final int W_EMERGE_RELEASE=20;
    public static final int W_BREAK=21;
    public static final int W_AUTO=22;
    public static final int W_MAN=23;
    public static final int W_CONSAT_RPM=24;
    public static final int W_COMB=25;
    public static final int W_STERV_VIEW=26;
    public static final int W_WINCH_LEVEL=27;

    // Camera Panel Commands
    public static final int C_LEFT_UP=28;
    public static final int C_LEFT=29;
    public static final int C_LEFT_DOWN=30;
    public static final int C_UP=31;
    public static final int C_DOWN=32;
    public static final int C_RIGHT_UP=33;
    public static final int C_RIGHT=34;
    public static final int C_RIGHT_DOWN=35;
    public static final int C_PORT_WING=36;
    public static final int C_STBD_WING=37;
    public static final int C_BRDG=38;
    public static final int C_PITCH=39;
    public static final int C_RESET=40;



}
