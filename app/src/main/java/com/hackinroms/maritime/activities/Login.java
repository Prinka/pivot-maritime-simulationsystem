package com.hackinroms.maritime.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hackinroms.maritime.R;

/**
 * Created by USER on 7/8/2016.
 */
public class Login extends AppCompatActivity {

    private static final String TAG = Login.class.getSimpleName();
    private Button btnLogin;

    private EditText loginPasswordTxt;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);


        btnLogin=(Button)findViewById(R.id.btn_login);

        loginPasswordTxt=(EditText)findViewById(R.id.login_password);

//        loginPasswordTxt.setText("012345678");
        // login Button press then varify the password.
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String PASSWORD=loginPasswordTxt.getText().toString();
                if(PASSWORD.isEmpty()){
                    loginPasswordTxt.setError("Please enter password");
                }else{
                    if( !PASSWORD.equals("012345678")){
                        Toast.makeText(Login.this, "Wrong password", Toast.LENGTH_LONG).show();
                    }else{
                        Toast.makeText(Login.this, "Login successful!", Toast.LENGTH_LONG).show();
                        Intent ii=new Intent(Login.this, UpdateActivity.class);
                        startActivity(ii);
                        finish();
                    }
                }
            }
        });
    }
}
