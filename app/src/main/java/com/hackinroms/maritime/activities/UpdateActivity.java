package com.hackinroms.maritime.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import com.hackinroms.maritime.R;
import com.hackinroms.maritime.application.MyApplication;

public class UpdateActivity extends AppCompatActivity implements View.OnClickListener{

    private CheckBox cb_winchControl, cb_camera,cb_thruster;
    private Button btn_update,btn_done;
    private boolean isWinchControl,iscamera,isThruster;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        
        xmlViews(); // Loads XML views
        RegisterListenerOnViews(); // Register listener on views

        try {
            isWinchControl=isWinchControl();
            iscamera=isCamera();
            isThruster=isThruster();
            cb_winchControl.setChecked(isWinchControl);
            cb_camera.setChecked(iscamera);
            cb_thruster.setChecked(isThruster);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void xmlViews() {
        cb_winchControl = (CheckBox) findViewById(R.id.update_cb_winchControl);
        cb_camera = (CheckBox) findViewById(R.id.update_cb_camera);
        cb_thruster = (CheckBox) findViewById(R.id.update_cb_thruster);

        btn_update = (Button) findViewById(R.id.update_btn_update);
        btn_done = (Button) findViewById(R.id.update_btn_done);
    }

    private void RegisterListenerOnViews() {

        btn_update.setOnClickListener(this);
        btn_done.setOnClickListener(this);

        cb_winchControl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    Toast.makeText(UpdateActivity.this,"winch control checked", Toast.LENGTH_SHORT).show();
//                    setWinchControl(true);
                    isWinchControl=true;
                }else {
                    isWinchControl=false;
//                    setWinchControl(false);
//                    Toast.makeText(UpdateActivity.this,"winch control unchecked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cb_camera.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    setCamera(true);
//                    Toast.makeText(UpdateActivity.this,"camera checked", Toast.LENGTH_SHORT).show();
                    iscamera=true;
                }else {
                    iscamera=false;
//                    setCamera(false);
//                    Toast.makeText(UpdateActivity.this,"camera unchecked", Toast.LENGTH_SHORT).show();
                }
            }
        });

        cb_thruster.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
//                    setThruster(true);
//                    Toast.makeText(UpdateActivity.this,"thruster checked", Toast.LENGTH_SHORT).show();
                    isThruster=true;
                }else {
                    isThruster=false;
//                    setThruster(false);
//                    Toast.makeText(UpdateActivity.this,"thruster unchecked", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.update_btn_update:
                if (!isWinchControl && !iscamera && !isThruster){
                    Toast.makeText(UpdateActivity.this,"please atleast one panel select.", Toast.LENGTH_SHORT).show();
                }else {
                    setWinchControl(isWinchControl);
                    setCamera(iscamera);
                    setThruster(isThruster);
                    Toast.makeText(UpdateActivity.this,"update successfully", Toast.LENGTH_SHORT).show();
                    Intent ii=new Intent(UpdateActivity.this, DeviceListActivity.class);
                    startActivity(ii);
                    finish();
                }


                break;
            case R.id.update_btn_done:
//                Toast.makeText(UpdateActivity.this,"Done", Toast.LENGTH_SHORT).show();
                if (!isWinchControl && !iscamera && !isThruster){
                    Toast.makeText(UpdateActivity.this,"please atleast one panel select.", Toast.LENGTH_SHORT).show();
                }else {
                    Intent ii=new Intent(UpdateActivity.this, MainActivity.class);
                    startActivity(ii);
                    finish();
                }

                break;
        }
    }



    private void setWinchControl(boolean value){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("winchControl", value);
        editor.commit();

    }
    private void setCamera(boolean value){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("camera", value);
        editor.commit();

    }

    private void setThruster(boolean value){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("thruster", value);
        editor.commit();

    }

    private boolean isWinchControl(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("winchControl", false);
    }

    private boolean isCamera(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("camera", false);
    }

    private boolean isThruster(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("thruster", false);
    }


}

