package com.hackinroms.maritime.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.hackinroms.maritime.R;
import com.hackinroms.maritime.connecter.ConnectedThread;
import com.hackinroms.maritime.fragments.CameraControlFragment;
import com.hackinroms.maritime.fragments.ThrusterFragment;
import com.hackinroms.maritime.fragments.WinchFragment;
import com.hackinroms.maritime.utility.CircularViewPagerHandler;
import com.hackinroms.maritime.utility.CustomViewPager;
import com.hackinroms.maritime.utility.FragmentLifecycle;
import com.hackinroms.maritime.utility.SwipeDirection;
import com.hackinroms.maritime.utility.ViewPagerSwapped;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

//    public static TabLayout tabLayout;
    public static CustomViewPager viewPager;
    private int currentPosition ;
    private int mScrollState;
    ViewPagerAdapter adapter;

    ArrayList<Integer> panelsList=new ArrayList<>();
//    int count=0;


//    Handler bluetoothIn;
    final int handlerState = 0;        				 //used to identify handler message
    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private StringBuilder recDataString = new StringBuilder();

    public ConnectedThread mConnectedThread;

    // SPP UUID service - this should work for most devices
//  private static final UUID BTMODULEUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    // Unique UUID for this application
    private static final UUID BTMODULEUUID = UUID.fromString("04c6093b-0000-1000-8000-00805f9b34fb");

    // String for MAC address
    private static String address;

    public ConnectedThread getConnectedThread(){
        return mConnectedThread;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        panelsList.clear();
//        count=0;

//        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        viewPager.setAllowedSwipeDirection(SwipeDirection.right);

        prepareArrayForPanels();
        setupViewPager(viewPager);
//        tabLayout.setupWithViewPager(viewPager);
        btAdapter = BluetoothAdapter.getDefaultAdapter();       // get Bluetooth adapter
        checkBTState();


    }

    private void setupViewPager(final CustomViewPager viewPager) {

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        loadPanels();
        viewPager.setAdapter(adapter);

//        viewPager.setOnPageChangeListener(new CircularViewPagerHandler(viewPager));

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int i, final float v, final int i2) {


//                Toast.makeText(MainActivity.this," position =  "+i,Toast.LENGTH_SHORT).show();
                ViewPagerSwapped fragment = (ViewPagerSwapped) adapter.instantiateItem(viewPager, i);
                if (fragment != null) {
                    fragment.fragmentBecameVisible();
                }
            }
            @Override
            public void onPageSelected(final int i) {


                FragmentLifecycle fragmentToShow = (FragmentLifecycle)adapter.getItem(i);
                fragmentToShow.onResumeFragment();

                FragmentLifecycle fragmentToHide = (FragmentLifecycle)adapter.getItem(currentPosition);
                fragmentToHide.onPauseFragment();

                currentPosition = i;

//                if(i == adapter.getCount()-1){
//                    viewPager.setCurrentItem(0, true);
//                }
            }
            @Override
            public void onPageScrollStateChanged(final int i) {

                handleScrollState(i);
                mScrollState = i;
            }
        });
    }

    private void handleScrollState(final int state) {
        if (state == ViewPager.SCROLL_STATE_IDLE) {
            setNextItemIfNeeded();
        }
    }

    private void setNextItemIfNeeded() {
        if (!isScrollStateSettling()) {
            handleSetNextItem();
        }
    }

    private boolean isScrollStateSettling() {
        return mScrollState == ViewPager.SCROLL_STATE_SETTLING;
    }

    private void handleSetNextItem() {
        final int lastPosition = viewPager.getAdapter().getCount() - 1;
        if(currentPosition == 0) {
            viewPager.setCurrentItem(lastPosition, false);
        } else if(currentPosition == lastPosition) {
            viewPager.setCurrentItem(0, false);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<String> mFragmentTitleList = new ArrayList();
        private final List<Fragment> mfragmentList = new ArrayList();

        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment getItem(int position) {
            return this.mfragmentList.get(position);
        }

        public int getCount() {
            return this.mfragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            this.mfragmentList.add(fragment);
            this.mFragmentTitleList.add(title);
        }

        public CharSequence getPageTitle(int position) {
            return this.mFragmentTitleList.get(position);
        }
    }


    private boolean isWinchControl(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("winchControl", false);
    }

    private boolean isCamera(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("camera", false);
    }

    private boolean isThruster(){
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPrefPanels", MODE_PRIVATE);
        return  pref.getBoolean("thruster", false);
    }


    private void prepareArrayForPanels(){

        if (isThruster()){
            panelsList.add(1);
        }

        if (isWinchControl()){
            panelsList.add(2);
        }

        if (isCamera()){
            panelsList.add(3);
        }

        if (!isWinchControl() && !isCamera() && !isThruster()){
            panelsList.add(1);
        }
    }

    private void loadPanels(){

        if (panelsList.size()!=0){

            if (panelsList.size()==1){

//                int item = panelsList.get(counter);
                for (int item: panelsList){

                    if (item==1){
                        adapter.addFrag(new ThrusterFragment(), "Thruster Panel");
//                    txt_title.setText(getString(R.string.panel_thurster));
                    }else if (item==2){
                        adapter.addFrag(new WinchFragment(), "Winch Panel");
//                    txt_title.setText(getString(R.string.panel_winchControl));
                    }else if (item == 3){
                        adapter.addFrag(new CameraControlFragment(), "Camera Control Panel");
//                    txt_title.setText(getString(R.string.panel_camera));
                    }
                }

                return;
            }

            if (panelsList.size()==2){

//                int item = panelsList.get(counter);
                for (int item: panelsList) {
                    if (item == 1) {
                        adapter.addFrag(new ThrusterFragment(), "Thruster Panel");
//                    txt_title.setText(getString(R.string.panel_thurster));
                    } else if (item == 2) {
                        adapter.addFrag(new WinchFragment(), "Winch Panel");
//                    txt_title.setText(getString(R.string.panel_winchControl));
                    } else if (item == 3) {
                        adapter.addFrag(new CameraControlFragment(), "Camera Control Panel");
//                    txt_title.setText(getString(R.string.panel_camera));
                    }

                }
                return;
            }

            if (panelsList.size()==3){
//                int item = panelsList.get(counter);
                for (int item: panelsList) {
                    if (item == 1) {
                        adapter.addFrag(new ThrusterFragment(), "Thruster Panel");
//                    txt_title.setText(getString(R.string.panel_thurster));
                    } else if (item == 2) {
                        adapter.addFrag(new WinchFragment(), "Winch Panel");
//                    txt_title.setText(getString(R.string.panel_winchControl));
                    } else if (item == 3) {
                        adapter.addFrag(new CameraControlFragment(), "Camera Control Panel");
//                    txt_title.setText(getString(R.string.panel_camera));
                    }
                }
                return;
            }
        }
    }




    private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {

        return  device.createRfcommSocketToServiceRecord(BTMODULEUUID);
        //creates secure outgoing connecetion with BT device using UUID
    }

    @Override
    public void onResume() {
        super.onResume();

        //Get MAC address from DeviceListActivity via intent
        Intent intent = getIntent();

        //Get the MAC address from the DeviceListActivty via EXTRA
        address = intent.getStringExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS);

        //create device and set the MAC address
        BluetoothDevice device = btAdapter.getRemoteDevice(address);

        try {
            btSocket = createBluetoothSocket(device);
        } catch (IOException e) {
            Toast.makeText(getBaseContext(), "Socket creation failed", Toast.LENGTH_LONG).show();
        }
        // Establish the Bluetooth socket connection.
        try
        {
            btSocket.connect();
        } catch (IOException e) {
            try
            {
                btSocket.close();
            } catch (IOException e2)
            {
                //insert code to deal with this
            }
        }
        mConnectedThread = new ConnectedThread(btSocket);
        mConnectedThread.start();

        //I send a character when resuming.beginning transmission to check device is connected
        //If it is not an exception will be thrown in the write method and finish() will be called
//        mConnectedThread.write("x");
    }

    @Override
    public void onPause()
    {
        super.onPause();
        try
        {
            //Don't leave Bluetooth sockets open when leaving activity
            btSocket.close();
        } catch (IOException e2) {
            //insert code to deal with this
        }
    }

    //Checks that the Android device Bluetooth is available and prompts to be turned on if off
    private void checkBTState() {

        if(btAdapter==null) {
            Toast.makeText(getBaseContext(), "Device does not support bluetooth", Toast.LENGTH_LONG).show();
        } else {
            if (btAdapter.isEnabled()) {
            } else {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
    }

}
