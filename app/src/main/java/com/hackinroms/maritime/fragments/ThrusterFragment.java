package com.hackinroms.maritime.fragments;

/**
 * Created by USER on 7/8/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import com.hackinroms.maritime.activities.MainActivity;
import com.hackinroms.maritime.connecter.Commands;
import com.hackinroms.maritime.utility.FragmentLifecycle;
import com.hackinroms.maritime.activities.Login;
import com.hackinroms.maritime.application.MyApplication;
import com.hackinroms.maritime.R;
import com.hackinroms.maritime.utility.ViewPagerSwapped;

public class ThrusterFragment  extends Fragment implements View.OnClickListener,FragmentLifecycle,ViewPagerSwapped {

    private Button btn_bowRedFull,btn_bowRedHalf,btn_bowStop,btn_bowGreenHalf,btn_bowGreenFull,
            btn_motorsStop,btn_motorsStart,btn_sternRedFull,btn_sternRedHalf,btn_sternStop,
            btn_sternGreenHalf,btn_sternGreenFull,btn_settings;

    Vibrator vibe ;
    int vibrateTime=100,bowCount=0,strenCount=0;
    private boolean isMotorStart=false;
    private SeekBar sb_helm,sb_leftTelegraph,sb_RightTelegraph;
    private String TAG="Truster";
    boolean isHelmFirst=true,isleftTelegraph=true,isrighttTelegraph=true;;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.thruster_fragment, null);
        isHelmFirst=true;
        isleftTelegraph=true;
        isrighttTelegraph=true;

         vibe = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);
        xmlViews(view); // Loads XML views
        RegisterListenerOnViews(); // Register listener on views

        bowButtons(0);
        motorButtons(1);
        sternButtons(0);

        return view;
    }



    private void xmlViews(View view) {

        btn_bowRedFull = (Button) view.findViewById(R.id.fragmentTruster_btn_bowRedFull);
        btn_bowRedHalf = (Button) view.findViewById(R.id.fragmentTruster_btn_bowRedHalf);
        btn_bowStop = (Button) view.findViewById(R.id.fragmentTruster_btn_bowStop);
        btn_bowGreenHalf = (Button) view.findViewById(R.id.fragmentTruster_btn_bowGreenHalf);
        btn_bowGreenFull = (Button) view.findViewById(R.id.fragmentTruster_btn_bowGreenFull);
        btn_motorsStop = (Button) view.findViewById(R.id.fragmentTruster_btn_motorsStop);
        btn_motorsStart = (Button) view.findViewById(R.id.fragmentTruster_btn_motorsStart);
        btn_sternRedFull = (Button) view.findViewById(R.id.fragmentTruster_btn_sternRedFull);
        btn_sternRedHalf = (Button) view.findViewById(R.id.fragmentTruster_btn_sternRedHalf);
        btn_sternStop = (Button) view.findViewById(R.id.fragmentTruster_btn_sternStop);
        btn_sternGreenHalf = (Button) view.findViewById(R.id.fragmentTruster_btn_sternGreenHalf);
        btn_sternGreenFull = (Button) view.findViewById(R.id.fragmentTruster_btn_sternGreenFull);
        btn_settings = (Button) view.findViewById(R.id.layoutTelepraph_btn_settings);

        sb_helm = (SeekBar) view.findViewById(R.id.layoutHelm_seekbar);
        sb_leftTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_red);
        sb_RightTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_green);

        final int helm=((MyApplication) getActivity().getApplication()).getHelm();
        int leftTelegraph= ((MyApplication) getActivity().getApplication()).getLeftTelegraph();
        int rightTelegraph= ((MyApplication) getActivity().getApplication()).getRightTelegraph();



        sb_helm.setProgress(helm);
        sb_leftTelegraph.setProgress(leftTelegraph);
        sb_RightTelegraph.setProgress(rightTelegraph);

        Log.v("Akeel", "Truster Helm : "+helm +"  leftTelegraph : "+ leftTelegraph + " rightTelegraph : "+ rightTelegraph);


        sb_helm.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (isHelmFirst){
                    sb_helm.setProgress(helm);
                    isHelmFirst=false;
                }else {
                    sb_helm.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setHelm(progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.HELM+progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.HELM);

                }

//                Log.v("Akeel","CameraControlFragment: Helm = "+progress);

            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.HELM);
            }
        });

        sb_leftTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);
//                sb_leftTelegraph.setProgress(progress);
                if (isleftTelegraph){
                    sb_leftTelegraph.setProgress(helm);
                    isleftTelegraph=false;
                }else {
                    sb_leftTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.LEFT_TELEGRAPH+progress);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.LEFT_TELEGRAPH);
            }
        });

        sb_RightTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);
//                sb_RightTelegraph.setProgress(progress);
                if (isrighttTelegraph){
                    sb_RightTelegraph.setProgress(helm);
                    isrighttTelegraph=false;
                }else {
                    sb_RightTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.RIGHT_TELEGRAPH+progress);

                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.RIGHT_TELEGRAPH);
            }
        });

    }

    private void RegisterListenerOnViews() {
        btn_bowRedFull.setOnClickListener(this);
        btn_bowRedHalf.setOnClickListener(this);
        btn_bowStop.setOnClickListener(this);
        btn_bowGreenHalf.setOnClickListener(this);
        btn_bowGreenFull.setOnClickListener(this);
        btn_motorsStop.setOnClickListener(this);
        btn_motorsStart.setOnClickListener(this);
        btn_sternRedFull.setOnClickListener(this);
        btn_sternRedHalf.setOnClickListener(this);
        btn_sternStop.setOnClickListener(this);
        btn_sternGreenHalf.setOnClickListener(this);
        btn_sternGreenFull.setOnClickListener(this);
        btn_settings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragmentTruster_btn_bowRedFull:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.T_BOW_RED_FULL);
                vibe.vibrate(vibrateTime);
                bowCount=1;
                if (isMotorStart){
                    bowButtons(1);
                }

                break;
            case R.id.fragmentTruster_btn_bowRedHalf:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_BOW_RED_HALF);
                vibe.vibrate(vibrateTime);
                bowCount=2;
                if (isMotorStart){
                    bowButtons(2);
                }
//                bowButtons(2);
                break;
            case R.id.fragmentTruster_btn_bowStop:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_BOW_STOP);
                vibe.vibrate(vibrateTime);
                bowCount=3;
                if (isMotorStart){
                    bowButtons(3);
                }
//                bowButtons(3);
                break;
            case R.id.fragmentTruster_btn_bowGreenHalf:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_BOW_GREEN_HALF);
                vibe.vibrate(vibrateTime);
                bowCount=4;
                if (isMotorStart){
                    bowButtons(4);
                }
//                bowButtons(4);
                break;
            case R.id.fragmentTruster_btn_bowGreenFull:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_BOW_GREEN_FULL);
                vibe.vibrate(vibrateTime);
                bowCount=5;
                if (isMotorStart){
                    bowButtons(5);
                }
//                bowButtons(5);
                break;
            case R.id.fragmentTruster_btn_motorsStop:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_MOTOR_STOP);
                vibe.vibrate(vibrateTime);
                isMotorStart=false;
                motorButtons(1);

                bowTimer(bowCount);
                strenTimer(strenCount);

                break;
            case R.id.fragmentTruster_btn_motorsStart:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_MOTOR_START);
                vibe.vibrate(vibrateTime);
                isMotorStart=true;
                motorButtons(2);
                bowButtons(3);
                sternButtons(3);

                break;
            case R.id.fragmentTruster_btn_sternRedFull:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_STERN_RED_FULL);
                vibe.vibrate(vibrateTime);
                strenCount=1;
                if (isMotorStart){
                    sternButtons(1);
                }
//                sternButtons(1);
                break;
            case R.id.fragmentTruster_btn_sternRedHalf:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_STERN_RED_HALF);
                vibe.vibrate(vibrateTime);
                strenCount=2;
                if (isMotorStart){
                    sternButtons(2);
                }
//                sternButtons(2);
                break;
            case R.id.fragmentTruster_btn_sternStop:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_STERN_STOP);
                vibe.vibrate(vibrateTime);
                strenCount=3;
                if (isMotorStart){
                    sternButtons(3);
                }
//                sternButtons(3);
                break;
            case R.id.fragmentTruster_btn_sternGreenHalf:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_STERN_GREEN_HALF);
                vibe.vibrate(vibrateTime);
                strenCount=4;
                if (isMotorStart){
                    sternButtons(4);
                }
//                sternButtons(4);
                break;
            case R.id.fragmentTruster_btn_sternGreenFull:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.T_STERN_GREEN_FULL);
                vibe.vibrate(vibrateTime);
                strenCount=5;
                if (isMotorStart){
                    sternButtons(5);
                }
//                sternButtons(5);
                break;
            case R.id.layoutTelepraph_btn_settings:
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finish();
                break;

            default:
        }

    }

    private void bowButtons(int buttonNo){

        switch (buttonNo){

            case 0:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);

                break;
            case 1:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_selected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);

                break;
            case 2:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_selected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 3:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_selected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 4:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_selected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 5:
                btn_bowRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bowStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_bowGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_bowGreenFull.setBackgroundResource(R.drawable.btn_green_selected);
                break;

        }

    }

    private void motorButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_motorsStop.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_motorsStart.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_motorsStop.setBackgroundResource(R.drawable.btn_red_selected);
                btn_motorsStart.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_motorsStop.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_motorsStart.setBackgroundResource(R.drawable.btn_green_selected);
                break;

        }

    }

    private void sternButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_selected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_selected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 3:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_selected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 4:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_selected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 5:
                btn_sternRedFull.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternRedHalf.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_sternStop.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_sternGreenHalf.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_sternGreenFull.setBackgroundResource(R.drawable.btn_green_selected);
                break;
        }
    }




    private void bowTimer(final int buttonCount){

        new CountDownTimer(500, 500) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
//                        mTextField.setText("done!");
                switch (buttonCount){
                    case 0:
                        bowButtons(0);
                        break;
                    case 1:
                        bowButtons(2);
                        bowTimer(2);
                        break;
                    case 2:
                        bowButtons(3);
                        bowTimer(3);
                        break;
                    case 3:
                        bowButtons(0);
                        break;
                    case 4:
                        bowButtons(3);
                        bowTimer(3);
                        break;
                    case 5:
                        bowButtons(4);
                        bowTimer(4);
                        break;
                }
            }
        }.start();
    }


    private void strenTimer(final int buttonCount){

        new CountDownTimer(500, 500) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
//                        mTextField.setText("done!");
                switch (buttonCount){
                    case 0:
                        sternButtons(0);
                        break;
                    case 1:
                        sternButtons(2);
                        strenTimer(2);
                        break;
                    case 2:
                        sternButtons(3);
                        strenTimer(3);
                        break;
                    case 3:
                        sternButtons(0);
                        bowCount=0;
                        strenCount=0;
                        break;
                    case 4:
                        sternButtons(3);
                        strenTimer(3);
                        break;
                    case 5:
                        sternButtons(4);
                        strenTimer(4);
                        break;
                }
            }
        }.start();
    }



    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();
    try {
        sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
        sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
        sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
    }catch (Exception e){
        e.printStackTrace();
    }
    }

    @Override
    public void fragmentBecameVisible() {
        sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
        sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
        sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
    }
}
