package com.hackinroms.maritime.fragments;

/**
 * Created by USER on 7/8/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.hackinroms.maritime.activities.MainActivity;
import com.hackinroms.maritime.connecter.Commands;
import com.hackinroms.maritime.utility.FragmentLifecycle;
import com.hackinroms.maritime.activities.Login;
import com.hackinroms.maritime.application.MyApplication;
import com.hackinroms.maritime.R;
import com.hackinroms.maritime.utility.ViewPagerSwapped;

public class CameraControlFragment extends Fragment implements View.OnClickListener, FragmentLifecycle,ViewPagerSwapped {

    private ImageView img_right,img_left,img_up,img_down,img_rightUp,img_rightDown,img_leftUp,img_LeftDown,img_camera;
    private Button btn_portWing,btn_bdrg,btn_stbdWing,btn_pitch,btn_reset,btn_settings;
    private SeekBar sb_helm,sb_leftTelegraph,sb_RightTelegraph;

    Vibrator vibe ;
    int vibrateTime=100;
    int start_x=0,end_x=0,start_y=0,end_y=0;
    boolean isPitch=false,isReset=false;
    private String TAG="Camera";

    boolean isHelmFirst=true,isleftTelegraph=true,isrighttTelegraph=true;;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.camera_fragment, null);
        isHelmFirst=true;
        isleftTelegraph=true;
        isrighttTelegraph=true;

        vibe = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);

        xmlViews(view); // Loads XML views
        RegisterListenerOnViews(); // Register listener on views

        optionButtons(2);
        return view;
    }



    private void xmlViews(View view) {

        img_right = (ImageView) view.findViewById(R.id.pointView_img_right);
        img_left = (ImageView) view.findViewById(R.id.pointView_img_left);
        img_up = (ImageView) view.findViewById(R.id.pointView_img_up);
        img_down = (ImageView) view.findViewById(R.id.pointView_img_down);
        img_rightUp = (ImageView) view.findViewById(R.id.pointView_img_rightUp);
        img_rightDown = (ImageView) view.findViewById(R.id.pointView_img_rightDown);
        img_leftUp = (ImageView) view.findViewById(R.id.pointView_img_leftUp);
        img_LeftDown = (ImageView) view.findViewById(R.id.pointView_img_leftDown);
        img_camera = (ImageView) view.findViewById(R.id.layoutCameraView_img_camera);

        btn_portWing = (Button) view.findViewById(R.id.pointView_btn_pointWing);
        btn_bdrg = (Button) view.findViewById(R.id.pointView_btn_BRDG);
        btn_stbdWing = (Button) view.findViewById(R.id.pointView_btn_stbdWing);

        btn_pitch = (Button) view.findViewById(R.id.layoutCameraView_btn_pitch);
        btn_reset = (Button) view.findViewById(R.id.layoutCameraView_btn_reset);
        btn_settings = (Button) view.findViewById(R.id.layoutTelepraph_btn_settings);

        sb_helm = (SeekBar) view.findViewById(R.id.layoutHelm_seekbar);
        sb_leftTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_red);
        sb_RightTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_green);

        final int helm=((MyApplication) getActivity().getApplication()).getHelm();
        int leftTelegraph= ((MyApplication) getActivity().getApplication()).getLeftTelegraph();
        int rightTelegraph= ((MyApplication) getActivity().getApplication()).getRightTelegraph();


        sb_helm.setProgress(helm);
        sb_leftTelegraph.setProgress(leftTelegraph);
        sb_RightTelegraph.setProgress(rightTelegraph);


        Log.v("Akeel", "Camera Helm : "+helm +"  leftTelegraph : "+ leftTelegraph + " rightTelegraph : "+ rightTelegraph);

        sb_helm.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (isHelmFirst){
                    sb_helm.setProgress(helm);
                    isHelmFirst=false;
                }else {
                    sb_helm.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setHelm(progress);

                }

//                Log.v("Akeel","CameraControlFragment: Helm = "+progress);

            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.HELM);
            }
        });

        sb_leftTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);
//                sb_leftTelegraph.setProgress(progress);
                if (isleftTelegraph){
                    sb_leftTelegraph.setProgress(helm);
                    isleftTelegraph=false;
                }else {
                    sb_leftTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);

                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.LEFT_TELEGRAPH);
            }
        });

        sb_RightTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);
//                sb_RightTelegraph.setProgress(progress);
                if (isrighttTelegraph){
                    sb_RightTelegraph.setProgress(helm);
                    isrighttTelegraph=false;
                }else {
                    sb_RightTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);

                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.RIGHT_TELEGRAPH);
            }
        });
    }

    private void RegisterListenerOnViews() {
        img_right.setOnClickListener(this);
        img_left.setOnClickListener(this);
        img_up.setOnClickListener(this);
        img_down.setOnClickListener(this);
        img_rightUp.setOnClickListener(this);
        img_rightDown.setOnClickListener(this);
        img_leftUp.setOnClickListener(this);
        img_LeftDown.setOnClickListener(this);

        btn_portWing.setOnClickListener(this);
        btn_bdrg.setOnClickListener(this);
        btn_stbdWing.setOnClickListener(this);

        btn_pitch.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_settings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pointView_img_right:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_RIGHT);
                vibe.vibrate(vibrateTime);
                arrowButtons(1);
                if (isPitch){
                    start_x =end_x;
                    end_x +=5;
                    moveObject(img_camera,start_x,end_x,start_y,end_y);
                }
                break;
            case R.id.pointView_img_left:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_LEFT);
                vibe.vibrate(vibrateTime);
                arrowButtons(2);
                if (isPitch){
                    start_x =end_x;
                    end_x -=5;
                    moveObject(img_camera,start_x,end_x,start_y,end_y);
                }
                break;
            case R.id.pointView_img_up:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_UP);
                vibe.vibrate(vibrateTime);
                arrowButtons(3);
                if (isPitch){
                    start_y =end_y;
                    end_y -=5;
                    moveObject(img_camera,start_x,end_x,start_y,end_y);
                }
                break;
            case R.id.pointView_img_down:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_DOWN);
                vibe.vibrate(vibrateTime);
                arrowButtons(4);
                if (isPitch){
                    start_y =end_y;
                    end_y +=5;
                    moveObject(img_camera,start_x,end_x,start_y,end_y);
                }
                break;
            case R.id.pointView_img_rightUp:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_RIGHT_UP);
                vibe.vibrate(vibrateTime);
                arrowButtons(5);
                break;
            case R.id.pointView_img_rightDown:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_RIGHT_DOWN);
                vibe.vibrate(vibrateTime);
                arrowButtons(6);
                break;
            case R.id.pointView_img_leftUp:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_LEFT_UP);
                vibe.vibrate(vibrateTime);
                arrowButtons(7);
                break;
            case R.id.pointView_img_leftDown:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_LEFT_DOWN);
                vibe.vibrate(vibrateTime);
                arrowButtons(8);
                break;
            case R.id.pointView_btn_pointWing:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_PORT_WING);
                vibe.vibrate(vibrateTime);
                optionButtons(1);
                break;
            case R.id.pointView_btn_BRDG:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_BRDG);
                vibe.vibrate(vibrateTime);
                optionButtons(2);
                break;
            case R.id.pointView_btn_stbdWing:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_STBD_WING);
                vibe.vibrate(vibrateTime);
                optionButtons(3);
                break;
            case R.id.layoutCameraView_btn_pitch:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_PITCH);
                if (isPitch){
                    isPitch=false;
                    btn_pitch.setBackgroundResource(R.drawable.btn_yellow_unselected);
                }else {
                    isPitch=true;
                    btn_pitch.setBackgroundResource(R.drawable.btn_yellow_selected);
                }
                break;
            case R.id.layoutCameraView_btn_reset:

                if (isReset){
                    isReset=false;
                    btn_reset.setBackgroundResource(R.drawable.btn_yellow_unselected);
                }else {
                    isReset=true;
                    btn_reset.setBackgroundResource(R.drawable.btn_yellow_selected);
                    start_x=0;
                    end_x=0;
                    start_y =0;
                    end_y=0;
                    moveObject(img_camera,start_x,end_x,start_y,end_y);
                }
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.C_RESET);
                break;
            case R.id.layoutTelepraph_btn_settings:
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finish();
                break;

            default:
        }

    }

    private void arrowButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 1:
                img_right.setImageResource(R.drawable.arrow_right_selected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 2:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_selected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 3:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_selected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 4:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_selected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 5:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_selected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 6:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_selected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 7:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_selected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_unselected);
                break;
            case 8:
                img_right.setImageResource(R.drawable.arrow_right_unselected);
                img_left.setImageResource(R.drawable.arrow_left_unselected);
                img_up.setImageResource(R.drawable.arrow_up_unselected);
                img_down.setImageResource(R.drawable.arrow_bottom_unselected);
                img_rightUp.setImageResource(R.drawable.arrow_right_up_angle_unselected);
                img_rightDown.setImageResource(R.drawable.arrow_right_bottom_angle_unselected);
                img_leftUp.setImageResource(R.drawable.arrow_left_up_angel_unselected);
                img_LeftDown.setImageResource(R.drawable.arrow_left_bottom_angle_selected);
                break;

        }
    }

    private void optionButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_portWing.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bdrg.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_stbdWing.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_portWing.setBackgroundResource(R.drawable.btn_red_selected);
                btn_bdrg.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_stbdWing.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_portWing.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bdrg.setBackgroundResource(R.drawable.btn_yellow_selected);
                btn_stbdWing.setBackgroundResource(R.drawable.btn_green_unselected);

                break;
            case 3:
                btn_portWing.setBackgroundResource(R.drawable.btn_red_unselected);
                btn_bdrg.setBackgroundResource(R.drawable.btn_yellow_unselected);
                btn_stbdWing.setBackgroundResource(R.drawable.btn_green_selected);

                break;

        }
    }

    private void moveObject(View view,int start_x,int end_x,int start_y,int end_y ){
        TranslateAnimation anim = new TranslateAnimation(start_x,end_x,start_y,end_y);
        anim.setDuration(300);
        anim.setFillAfter(true);
        view.startAnimation(anim);

    }



    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment(): " + TAG, Toast.LENGTH_SHORT).show();
//        sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
//        sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
//        sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();
        try {
            sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
            sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
            sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void fragmentBecameVisible() {
        sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
        sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
        sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
    }
}
