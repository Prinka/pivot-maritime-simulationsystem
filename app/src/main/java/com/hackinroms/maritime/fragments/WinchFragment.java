package com.hackinroms.maritime.fragments;

/**
 * Created by USER on 7/8/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;

import com.hackinroms.maritime.activities.MainActivity;
import com.hackinroms.maritime.connecter.Commands;
import com.hackinroms.maritime.utility.FragmentLifecycle;
import com.hackinroms.maritime.activities.Login;
import com.hackinroms.maritime.application.MyApplication;
import com.hackinroms.maritime.R;
import com.hackinroms.maritime.utility.ViewPagerSwapped;

public class WinchFragment extends Fragment implements View.OnClickListener,FragmentLifecycle,ViewPagerSwapped {

    private Button btn_speed1,btn_speed2,btn_speed3,btn_speed4,btn_emergeRelease,btn_break,btn_auto,btn_manual,
                btn_consatRPM,btn_comb,btn_starvView,btn_settings;

    private Boolean isEmergeRelease=false,isBreak=false,isSternView=false;

    Vibrator vibe ;
    int vibrateTime=100;
    private SeekBar sb_helm,sb_leftTelegraph,sb_RightTelegraph;
    private String TAG="Which";
    boolean isHelmFirst=true,isleftTelegraph=true,isrighttTelegraph=true;;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.winch_fragment, null);
        isHelmFirst=true;
        isleftTelegraph=true;
        isrighttTelegraph=true;

        vibe = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);

        xmlViews(view); // Loads XML views
        RegisterListenerOnViews(); // Register listener on views

        return view;
    }



    private void xmlViews(View view) {
        btn_speed1 = (Button) view.findViewById(R.id.fragmentWhich_btn_speed1);
        btn_speed2 = (Button) view.findViewById(R.id.fragmentWhich_btn_speed2);
        btn_speed3 = (Button) view.findViewById(R.id.fragmentWhich_btn_speed3);
        btn_speed4 = (Button) view.findViewById(R.id.fragmentWhich_btn_speed4);
        btn_emergeRelease = (Button) view.findViewById(R.id.fragmentWhich_btn_emergeRelease);
        btn_break = (Button) view.findViewById(R.id.fragmentWhich_btn_break);
        btn_auto = (Button) view.findViewById(R.id.fragmentWhich_btn_auto);
        btn_manual = (Button) view.findViewById(R.id.fragmentWhich_btn_manual);
        btn_consatRPM = (Button) view.findViewById(R.id.fragmentWhich_btn_consatRPM);
        btn_comb = (Button) view.findViewById(R.id.fragmentWhich_btn_comb);
        btn_starvView = (Button) view.findViewById(R.id.fragmentWhich_btn_stervView);
        btn_settings = (Button) view.findViewById(R.id.layoutTelepraph_btn_settings);

        sb_helm = (SeekBar) view.findViewById(R.id.layoutHelm_seekbar);
        sb_leftTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_red);
        sb_RightTelegraph = (SeekBar) view.findViewById(R.id.layoutTelepraph_seekbar_green);

        final int helm=((MyApplication) getActivity().getApplication()).getHelm();
        int leftTelegraph= ((MyApplication) getActivity().getApplication()).getLeftTelegraph();
        int rightTelegraph= ((MyApplication) getActivity().getApplication()).getRightTelegraph();


        sb_helm.setProgress(helm);
        sb_leftTelegraph.setProgress(leftTelegraph);
        sb_RightTelegraph.setProgress(rightTelegraph);

        Log.v("Akeel", "Which Helm : "+helm +"  leftTelegraph : "+ leftTelegraph + " rightTelegraph : "+ rightTelegraph);

        sb_helm.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                if (isHelmFirst){
                    sb_helm.setProgress(helm);
                    isHelmFirst=false;
                }else {
                    sb_helm.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setHelm(progress);


                }

//                Log.v("Akeel","CameraControlFragment: Helm = "+progress);

            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(), Commands.HELM);
            }
        });

        sb_leftTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);
//                sb_leftTelegraph.setProgress(progress);
                if (isleftTelegraph){
                    sb_leftTelegraph.setProgress(helm);
                    isleftTelegraph=false;
                }else {
                    sb_leftTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setLeftTelegraph(progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.LEFT_TELEGRAPH+progress);
                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.LEFT_TELEGRAPH);
            }
        });

        sb_RightTelegraph.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);
//                sb_RightTelegraph.setProgress(progress);
                if (isrighttTelegraph){
                    sb_RightTelegraph.setProgress(helm);
                    isrighttTelegraph=false;
                }else {
                    sb_RightTelegraph.setProgress(progress);
                    ((MyApplication) getActivity().getApplication()).setRightTelegraph(progress);
//                    ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.RIGHT_TELEGRAPH+progress);

                }
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.RIGHT_TELEGRAPH);
            }
        });
    }

    private void RegisterListenerOnViews() {
        btn_speed1.setOnClickListener(this);
        btn_speed2.setOnClickListener(this);
        btn_speed3.setOnClickListener(this);
        btn_speed4.setOnClickListener(this);
        btn_emergeRelease.setOnClickListener(this);
        btn_break.setOnClickListener(this);
        btn_auto.setOnClickListener(this);
        btn_manual.setOnClickListener(this);
        btn_consatRPM.setOnClickListener(this);
        btn_comb.setOnClickListener(this);
        btn_starvView.setOnClickListener(this);
        btn_settings.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragmentWhich_btn_speed1:
                vibe.vibrate(vibrateTime);
                    speedButtons(1);
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_SPEED_1);
                break;
            case R.id.fragmentWhich_btn_speed2:
                vibe.vibrate(vibrateTime);
                speedButtons(2);
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_SPEED_2);
                break;
            case R.id.fragmentWhich_btn_speed3:
                vibe.vibrate(vibrateTime);
                speedButtons(3);
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_SPEED_3);
                break;
            case R.id.fragmentWhich_btn_speed4:
                vibe.vibrate(vibrateTime);
                speedButtons(4);
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_SPEED_4);
                break;
            case R.id.fragmentWhich_btn_emergeRelease:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_EMERGE_RELEASE);
                vibe.vibrate(vibrateTime);
                if (isEmergeRelease){
                   isEmergeRelease=false;
                    btn_emergeRelease.setBackgroundResource(R.drawable.btn_green_unselected);
                }else {
                    isEmergeRelease=true;
                    btn_emergeRelease.setBackgroundResource(R.drawable.btn_green_selected);
                }
                break;
            case R.id.fragmentWhich_btn_break:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_BREAK);
                vibe.vibrate(vibrateTime);
                if (isBreak){
                    isBreak=false;
                    btn_break.setBackgroundResource(R.drawable.btn_green_unselected);
                }else {
                    isBreak=true;
                    btn_break.setBackgroundResource(R.drawable.btn_green_selected);
                }
                break;
            case R.id.fragmentWhich_btn_auto:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_AUTO);
                vibe.vibrate(vibrateTime);
                autoManualButtons(1);
                break;
            case R.id.fragmentWhich_btn_manual:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_MAN);
                vibe.vibrate(vibrateTime);
                autoManualButtons(2);
                break;
            case R.id.fragmentWhich_btn_consatRPM:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_CONSAT_RPM);
                vibe.vibrate(vibrateTime);
                rpmButtons(1);
                break;
            case R.id.fragmentWhich_btn_comb:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_COMB);
                vibe.vibrate(vibrateTime);
                rpmButtons(2);
                break;
            case R.id.fragmentWhich_btn_stervView:
                ((MainActivity)getActivity()).getConnectedThread().write(getActivity(),Commands.W_STERV_VIEW);
                vibe.vibrate(vibrateTime);
                if (isSternView){
                    isSternView=false;
                    btn_starvView.setBackgroundResource(R.drawable.btn_green_unselected);
                }else {
                    isSternView=true;
                    btn_starvView.setBackgroundResource(R.drawable.btn_green_selected);
                }
                break;
            case R.id.layoutTelepraph_btn_settings:
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finish();
                break;

            default:
        }

    }

    private void speedButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_speed1.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed2.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed3.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed4.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_speed1.setBackgroundResource(R.drawable.btn_green_selected);
                btn_speed2.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed3.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed4.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_speed1.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed2.setBackgroundResource(R.drawable.btn_green_selected);
                btn_speed3.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed4.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 3:
                btn_speed1.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed2.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed3.setBackgroundResource(R.drawable.btn_green_selected);
                btn_speed4.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 4:
                btn_speed1.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed2.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed3.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_speed4.setBackgroundResource(R.drawable.btn_green_selected);
                break;

        }
    }

    private void emergencyButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_emergeRelease.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_break.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_emergeRelease.setBackgroundResource(R.drawable.btn_green_selected);
                btn_break.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_emergeRelease.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_break.setBackgroundResource(R.drawable.btn_green_selected);
                break;
        }
    }

    private void autoManualButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_auto.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_manual.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_auto.setBackgroundResource(R.drawable.btn_green_selected);
                btn_manual.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_auto.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_manual.setBackgroundResource(R.drawable.btn_green_selected);
                break;
        }
    }

    private void rpmButtons(int buttonNo){
        switch (buttonNo){
            case 0:
                btn_consatRPM.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_comb.setBackgroundResource(R.drawable.btn_green_unselected);
//                btn_starvView.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 1:
                btn_consatRPM.setBackgroundResource(R.drawable.btn_green_selected);
                btn_comb.setBackgroundResource(R.drawable.btn_green_unselected);
//                btn_starvView.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 2:
                btn_consatRPM.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_comb.setBackgroundResource(R.drawable.btn_green_selected);
//                btn_starvView.setBackgroundResource(R.drawable.btn_green_unselected);
                break;
            case 3:
                btn_consatRPM.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_comb.setBackgroundResource(R.drawable.btn_green_unselected);
                btn_starvView.setBackgroundResource(R.drawable.btn_green_selected);
                break;
        }
    }



    @Override
    public void onPauseFragment() {
        Log.i(TAG, "onPauseFragment()");
//        Toast.makeText(getActivity(), "onPauseFragment():" + TAG, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResumeFragment() {
        Log.i(TAG, "onResumeFragment()");
//        Toast.makeText(getActivity(), "onResumeFragment():" + TAG, Toast.LENGTH_SHORT).show();
        try {
            sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
            sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
            sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void fragmentBecameVisible() {
        sb_helm.setProgress(((MyApplication) getActivity().getApplication()).getHelm());
        sb_leftTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getLeftTelegraph());
        sb_RightTelegraph.setProgress(((MyApplication) getActivity().getApplication()).getRightTelegraph());
    }
}
