package com.hackinroms.maritime.application;

import android.app.Application;
import android.util.Log;

/**
 * Created by akeel on 10/08/2016.
 */
public class MyApplication extends Application{


    private int helm=50;
    private int leftTelegraph=50;
    private int rightTelegraph=50;

    public int getHelm() {
        return helm;
    }

    public void setHelm(int helm) {
        this.helm = helm;
        Log.v("Akeel","Helm = "+helm);
    }

    public int getLeftTelegraph() {
        return leftTelegraph;
    }

    public void setLeftTelegraph(int leftTelegraph) {
        this.leftTelegraph = leftTelegraph;
        Log.v("Akeel","leftTelegraph = "+leftTelegraph);
    }

    public int getRightTelegraph() {
        return rightTelegraph;
    }

    public void setRightTelegraph(int rightTelegraph) {
        this.rightTelegraph = rightTelegraph;
        Log.v("Akeel","rightTelegraph = "+rightTelegraph);
    }
}

