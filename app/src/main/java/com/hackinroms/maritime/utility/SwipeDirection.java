package com.hackinroms.maritime.utility;

/**
 * Created by akeel on 11/09/2016.
 */
public enum SwipeDirection {
    all, left, right, none ;
}
