package com.hackinroms.maritime.utility;

/**
 * Created by akeel on 10/08/2016.
 */
public interface FragmentLifecycle {
    public void onPauseFragment();
    public void onResumeFragment();
}
